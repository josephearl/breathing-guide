# Breathing Guide

Displays rise and fall of breaths.

![Screenshot](http://i.imgur.com/1wmAWBo.png)

### Requirements

- Android SDK

### Building

    ./gradlew clean build

The compiled app will be available in `app/build/outputs/apk`.