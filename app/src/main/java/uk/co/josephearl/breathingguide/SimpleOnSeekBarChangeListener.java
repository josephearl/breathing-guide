package uk.co.josephearl.breathingguide;

import android.widget.SeekBar;

abstract class SimpleOnSeekBarChangeListener implements SeekBar.OnSeekBarChangeListener {

    public SimpleOnSeekBarChangeListener() {
    }

    @Override
    public void onStartTrackingTouch(final SeekBar seekBar) {
    }

    @Override
    public void onStopTrackingTouch(final SeekBar seekBar) {
    }

}
