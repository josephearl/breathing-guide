package uk.co.josephearl.breathingguide;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.PixelFormat;
import android.graphics.PorterDuff;
import android.os.SystemClock;
import android.util.AttributeSet;
import android.view.SurfaceHolder;
import android.view.SurfaceView;

public class BreathingGuideView extends SurfaceView {

    private BreathingGuideRenderer renderer;

    public BreathingGuideView(final Context context) {
        super(context);
        init(context);
    }

    public BreathingGuideView(final Context context, final AttributeSet attrs) {
        super(context, attrs);
        init(context);
    }

    public BreathingGuideView(final Context context, final AttributeSet attrs, final int defStyle) {
        super(context, attrs, defStyle);
        init(context);
    }

    public float getRise() {
        return renderer.getRise();
    }

    public void setRise(final float rise) {
        renderer.setRise(rise);
    }

    public float getFall() {
        return renderer.getFall();
    }

    public void setFall(final float fall) {
        renderer.setFall(fall);
    }

    @Override
    protected void onDraw(final Canvas canvas) {
        super.onDraw(canvas);
        drawBreathingGuide(canvas, 0);
    }

    void drawBreathingGuide(final Canvas canvas, final long elapsed) {
        canvas.drawColor(Color.TRANSPARENT, PorterDuff.Mode.CLEAR);

        final int saveId = canvas.save();
        canvas.translate(getPaddingLeft(), getPaddingTop());

        final int availableWidth = getWidth() - getPaddingLeft() - getPaddingRight();
        final int availableHeight = getHeight() - getPaddingTop() - getPaddingBottom();
        renderer.increaseTime(elapsed);
        renderer.render(canvas, availableWidth, availableHeight);

        canvas.restoreToCount(saveId);
    }

    private void init(final Context context) {
        renderer = new BreathingGuideRenderer(context);
    }

}
