package uk.co.josephearl.breathingguide;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;

public class BreathingGuideRenderer {

    private static final float WAVE_STROKE_WIDTH = 1.0f;
    private static final float MARKER_STROKE_WIDTH = 2.0f;
    private static final float DEBUG_MARKER_STROKE_WIDTH = 0.5f;

    private final float density;
    private final float waveHeight;
    private final float densityScale;
    private final float widthOneSecond;
    private final float scaledWidthOneSecond;
    private final float timeMarkerHeight;
    private final Paint wavePaint;
    private final Paint markerPaint;
    private final Paint timeMarkerPaint;

    private float rise = 1.0f;
    private float fall = 4.0f;
    private float riseScale = 1.0f;
    private float fallScale = 0.25f;
    private float offset = 0.0f;

    public BreathingGuideRenderer(final Context context) {
        density = context.getResources().getDisplayMetrics().density;
        waveHeight = 100 * density;
        densityScale = 1.0f / density;
        widthOneSecond = 25;
        scaledWidthOneSecond = widthOneSecond * density;

        timeMarkerHeight = 10 * density;

        wavePaint = new Paint();
        wavePaint.setColor(Color.BLACK);
        wavePaint.setAntiAlias(true);
        wavePaint.setStrokeWidth(WAVE_STROKE_WIDTH * density);
        wavePaint.setStyle(Paint.Style.STROKE);

        markerPaint = new Paint();
        markerPaint.setAntiAlias(true);
        markerPaint.setStrokeWidth(MARKER_STROKE_WIDTH * density);
        markerPaint.setStyle(Paint.Style.STROKE);

        timeMarkerPaint = new Paint();
        timeMarkerPaint.setColor(Color.RED);
        timeMarkerPaint.setAntiAlias(true);
        timeMarkerPaint.setStrokeWidth(DEBUG_MARKER_STROKE_WIDTH * density);
        timeMarkerPaint.setStyle(Paint.Style.STROKE);
    }

    public synchronized float getRise() {
        return rise;
    }

    public synchronized void setRise(final float rise) {
        final float oldRise = this.rise;
        this.rise = rise;
        riseScale = 1.0f / rise;

        recalculateOffset(rise, fall, oldRise, fall);
    }

    public synchronized float getFall() {
        return fall;
    }

    public synchronized void setFall(final float fall) {
        final float oldFall = this.fall;
        this.fall = fall;
        fallScale = 1.0f / fall;

        recalculateOffset(rise, fall, rise, oldFall);
    }

    public synchronized void increaseTime(final long time) {
        final float offset = ((time / 1000.0f) + this.offset) % ((rise + fall) * 1000);
        this.offset = offset;
    }

    public synchronized void render(final Canvas canvas, final int width, final int height) {
        // Center vertically
        int saveId = canvas.save();
        canvas.translate(0, height / 2);

        drawBreathingGuideWave(canvas, width, height);
        drawTimeMarkers(canvas, width, height);

        canvas.restoreToCount(saveId);
    }

    public synchronized void reset() {
        offset = 0;
    }

    private void drawBreathingGuideWave(final Canvas canvas, final int width, final int height) {
        final double timeScale = 1.0f / widthOneSecond * Math.PI;
        final int pixelOffset = (int) (offset * scaledWidthOneSecond);
        final int halfWidth = width / 2;

        double angle = 0;
        for (int i = 0; i < pixelOffset; i++) {
            float sideScale = (angle <= Math.PI / 2 || angle > 3 * Math.PI / 2) ? fallScale : riseScale;
            double angleIncrease = sideScale * timeScale * densityScale;
            double angle1 = angle + angleIncrease;

            angle = angle1 % (Math.PI * 2);
        }

        drawWave(canvas, halfWidth, angle, halfWidth, 1);
        drawWave(canvas, halfWidth, angle, halfWidth, -1);
        drawWaveMarker(canvas, halfWidth, angle);
    }

    private void drawWave(final Canvas canvas, final int offset, final double theta, final int width, final int sign) {
        final double timeScale = 1.0f / widthOneSecond * Math.PI;

        double angle = theta;
        for (int i = 0; i < width; i++) {
            float sideScale = (angle <= Math.PI / 2 || angle > 3 * Math.PI / 2) ? fallScale : riseScale;
            double angleIncrease = sideScale * timeScale * densityScale;

            double angle0 = angle;
            double angle1 = angle0 + (sign * angleIncrease);

            float x0 = offset + (sign * i);
            float x1 = x0 + sign;

            float y0 = (float) (waveHeight * Math.sin(angle0));
            float y1 = (float) (waveHeight * Math.sin(angle1));

            canvas.drawLine(x0, y0, x1, y1, wavePaint);

            angle = (angle1 + (2 * Math.PI)) % (Math.PI * 2);
        }
    }

    private void drawWaveMarker(final Canvas canvas, final int offset, final double theta) {
        float x0 = offset;
        float y0 = (float) (waveHeight * Math.sin(theta));
        canvas.drawCircle(x0, y0, 20, markerPaint);
    }

    private void drawTimeMarkers(final Canvas canvas, final int width, final int height) {
        final int saveId = canvas.save();
        canvas.translate(width / 2.0f, 0);

        final int markersInHalfWidth = (int) (width / (2.0f * scaledWidthOneSecond));
        for (int i = 0; i <= markersInHalfWidth; i++) {
            float x0 = i * scaledWidthOneSecond;
            float y0 = -timeMarkerHeight;
            float y1 = timeMarkerHeight;
            canvas.drawLine(x0, y0, x0, y1, timeMarkerPaint);
        }
        for (int i = -1; i >= -markersInHalfWidth; i--) {
            float x0 = i * scaledWidthOneSecond;
            float y0 = -timeMarkerHeight;
            float y1 = timeMarkerHeight;
            canvas.drawLine(x0, y0, x0, y1, timeMarkerPaint);
        }

        canvas.restoreToCount(saveId);
    }

    private void recalculateOffset(final float newRise, final float newFall, final float oldRise, final float oldFall) {
        // offset = a * rise/2 + b * fall/2
        final float halfOldWavelength = (oldRise + oldFall) / 2;
        final int offsetHalfOldWavelengths = (int) (offset / halfOldWavelength);

        // Speed things up by working out the number of half wave lengths first
        float offset = this.offset;
        float rises = offsetHalfOldWavelengths;
        float falls = offsetHalfOldWavelengths;
        int quarters = offsetHalfOldWavelengths * 2;
        offset -= (offsetHalfOldWavelengths * halfOldWavelength);

        for (int i = 0; i < 2; i++) {
            if (offset <= 0.0f) {
                break;
            }
            boolean isFall = quarters % 4 == 0 || quarters % 4 == 3;
            float length = isFall ? oldFall / 2 : oldRise / 2;
            float increase = Math.min(offset / length, 1);
            if (isFall) {
                falls += increase;
            } else {
                rises += increase;
            }
            offset -= length;
            quarters++;
        }

        this.offset = (rises * (newRise / 2)) + (falls * (newFall / 2));
    }

}
