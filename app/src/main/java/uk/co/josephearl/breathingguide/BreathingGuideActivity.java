package uk.co.josephearl.breathingguide;

import android.graphics.Canvas;
import android.graphics.PixelFormat;
import android.os.SystemClock;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.SurfaceHolder;
import android.widget.SeekBar;
import android.widget.TextView;

public class BreathingGuideActivity extends ActionBarActivity {

    private BreathingGuideView breathingGuideView;
    private TextView riseSeekBarLabel;
    private SeekBar riseSeekBar;
    private TextView fallSeekBarLabel;
    private SeekBar fallSeekBar;

    private BreathingGuideRenderThread renderThread;

    public BreathingGuideActivity() {
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_breathing_guide);

        setupViews();
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.breathing_guide, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_about) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private void setupViews() {
        breathingGuideView = (BreathingGuideView) findViewById(R.id.breathingGuideView);
        breathingGuideView.setZOrderOnTop(true);
        SurfaceHolder surfaceHolder = breathingGuideView.getHolder();
        surfaceHolder.setFormat(PixelFormat.TRANSPARENT);
        surfaceHolder.addCallback(new BreathingGuideSurfaceHolderCallback());

        riseSeekBarLabel = (TextView) findViewById(R.id.riseSeekBarLabel);
        riseSeekBarLabel.setText(formatTime(breathingGuideView.getRise()));

        riseSeekBar = (SeekBar) findViewById(R.id.riseSeekBar);
        riseSeekBar.setProgress((int) (breathingGuideView.getRise() * 10 - 10));
        riseSeekBar.setOnSeekBarChangeListener(new SimpleOnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(final SeekBar seekBar, final int i, final boolean fromUser) {
                breathingGuideView.setRise(1 + i / 10.0f);
                riseSeekBarLabel.setText(formatTime(breathingGuideView.getRise()));
            }
        });

        fallSeekBarLabel = (TextView) findViewById(R.id.fallSeekBarLabel);
        fallSeekBarLabel.setText(formatTime(breathingGuideView.getFall()));

        fallSeekBar = (SeekBar) findViewById(R.id.fallSeekBar);
        fallSeekBar.setProgress((int) (breathingGuideView.getFall() * 10 - 10));
        fallSeekBar.setOnSeekBarChangeListener(new SimpleOnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(final SeekBar seekBar, final int i, final boolean fromUser) {
                breathingGuideView.setFall(1 + i / 10.0f);
                fallSeekBarLabel.setText(formatTime(breathingGuideView.getFall()));
            }
        });
    }

    private String formatTime(final float time) {
        return String.format("%.1fs", time);
    }

    private class BreathingGuideSurfaceHolderCallback implements SurfaceHolder.Callback {

        private BreathingGuideSurfaceHolderCallback() {
        }

        @Override
        public void surfaceCreated(final SurfaceHolder surfaceHolder) {
            renderThread = new BreathingGuideRenderThread(surfaceHolder);
            renderThread.start();
        }

        @Override
        public void surfaceChanged(final SurfaceHolder surfaceHolder, final int i, final int i2, final int i3) {

        }

        @Override
        public void surfaceDestroyed(final SurfaceHolder surfaceHolder) {
            if (renderThread != null) {
                renderThread.interrupt();
                renderThread = null;
            }
        }

    }

    private class BreathingGuideRenderThread extends Thread {

        private final SurfaceHolder surfaceHolder;

        private long lastDrawingTime;

        private BreathingGuideRenderThread(final SurfaceHolder surfaceHolder) {
            this.surfaceHolder = surfaceHolder;
            lastDrawingTime = SystemClock.uptimeMillis();
        }

        @Override
        public void run() {
            while (true) {
                Canvas canvas = surfaceHolder.lockCanvas();
                if (canvas != null) {
                    try {
                        long now = SystemClock.uptimeMillis();
                        long elapsed = now - lastDrawingTime;
                        breathingGuideView.drawBreathingGuide(canvas, elapsed);
                        lastDrawingTime = now;
                    } finally {
                        surfaceHolder.unlockCanvasAndPost(canvas);
                    }
                }
                try {
                    Thread.sleep(16);
                } catch (InterruptedException e) {
                    return;
                }
            }
        }

    }

}
